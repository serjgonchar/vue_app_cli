import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import record from "@/store/record";
import firebase from "firebase";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		error: null,
		userId: false
	},
	mutations: {
		setError(state, error) {
			state.error = error
		},
		clearError(state) {
			state.error = null
		},
		setUserId(state, userId) {
			console.log("mutation with", userId);
			state.userId = userId;
		}
	},
	getters: {
		error: s => s.error,
		getUid: () => {
			const  user = firebase.auth().currentUser;
			return user ? user.uid : null;
		},
	},
	modules: {
		auth, record
	}
})
