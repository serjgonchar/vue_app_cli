import Vue from 'vue';
import Router from 'vue-router';
import firebase from "firebase/app";

Vue.use(Router);

const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'home',
			meta: {layout: 'main'},
			component: () => import('./views/Home')
		},
		{
			path: '/edit',
			name: 'edit',
			meta: {layout: 'main', auth: true},
			component: () => import('./views/Edit')
		},
		{
			path: '/single/:id',
			name: 'single',
			meta: {layout: 'main'},
			component: () => import('./views/Single')
		},
		{
			path: '/login',
			name: 'login',
			meta: {layout: 'auth'},
			component: () => import('./views/Login.vue')
		},
		{
			path: '/registration',
			name: 'registration',
			meta: {layout: 'auth'},
			component: () => import('./views/Registration')
		},
	]
});

router.beforeEach((to, from, next) => {
	const currentUser = firebase.auth().currentUser
	const requireAuth = to.matched.some(record => record.meta.auth)

	if (requireAuth && !currentUser) {
		next('/login?message=login')
	} else {
		next()
	}
});

export default router