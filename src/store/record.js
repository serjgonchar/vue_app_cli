import firebase from "firebase/app"

export default {
	actions: {
		async createRecord({commit}, record) {
			try {
				return await firebase.database().ref('/records/').push(record)
			} catch (e) {
				commit('setError', e);
				throw e
			}
		},
		async removePost({commit}, record) {
			console.log(record);
			try {
				let ref = await firebase.database().ref('/records/' + record);
				ref.remove();
			} catch (e) {
				commit('setError', e);
				throw e
			}
		}
	}
}