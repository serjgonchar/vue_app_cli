export default {
	'auth/user-not-found' : 'Пользователь с таким Email не найден',
	'auth/wrong-password' : 'Неверный пароль',
	'auth/email-already-in-use' : 'Пользователь с таким Email уже существует'
}