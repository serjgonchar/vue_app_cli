import Vue from 'vue';
import Vuelidate from "vuelidate/src";
import App from './App.vue';
import router from "@/router";
import store from "./store";

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false;
Vue.use(Vuelidate);

let app;

firebase.initializeApp(
    {
      apiKey: "AIzaSyCN5KEuaq726yQi0EatRzURObFywHxjmOI",
      authDomain: "vue-cli-project-template.firebaseapp.com",
      databaseURL: "https://vue-cli-project-template.firebaseio.com",
      projectId: "vue-cli-project-template",
      storageBucket: "vue-cli-project-template.appspot.com",
      messagingSenderId: "191731002801",
      appId: "1:191731002801:web:9624da8b125da4fbe0e190",
      measurementId: "G-6WJYSPGR75"
    }
);
firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
});


// eslint-disable-next-line no-unused-vars
// let userStatus;
//
firebase.auth().onAuthStateChanged(function (user) {
    console.log("onAuthStateChanged: ", user);
    if (user) {
        store.commit('setUserId', true);
    } else {
        store.commit('setUserId', false);
    }
});